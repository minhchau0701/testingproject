package pages;


import org.openqa.selenium.ElementNotVisibleException;

import Cucumber_Hooks.*;



public class Homepage {

	
	public void navigateLogin () {
		
		Webelement.getElementbyXpath("loginLink").click();
		
	}
	
	public void navigateFooter (String item) {
		switch (item) {
		case "About Us":
			Webelement.getElementbyXpath("aboutusLink").click();
			break;
		case "Contact Us":
			Webelement.getElementbyXpath("contactusLink").click();
			break;
		case "FQA's":
			Webelement.getElementbyXpath("fqaLink").click();
			break;
		case "Special Products":
			Webelement.getElementbyXpath("speacialprodLink").click();
			break;
		default: 
			throw new ElementNotVisibleException(item);
		}
	}

	public void displayPage (String item) {
		switch (item) {
		case "About Us":
			Webelement.getElementbyXpath("aboutusHeaderPage");
			Webelement.getElementbyXpath("aboutusContent");
			break;
		case "Contact Us":
			Webelement.getElementbyXpath("contactMessage");
			Webelement.getElementbyXpath("contactInfo");
			break;
		case "FQA's":
			Webelement.getElementbyXpath("fqaHeader");
			break;
		case "Special Products":
			Webelement.getElementbyXpath("productTitle");
			break;
		default: 
			throw new ElementNotVisibleException(item);
		}
	}

}
