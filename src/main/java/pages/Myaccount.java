package pages;

import Cucumber_Hooks.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;

public class Myaccount {
	
	public void navigateAccountpage () {
		Webelement.getElementbyXpath("usernameHeader").click();
		Webelement.getElementbyXpath("menu_myaccountLink").click();
	}
	
	
	public void navigateSideMenu (String item) {
		switch (item) {
		case "Personal":
			Webelement.getElementbyXpath("personalBtn").click();
			break;
		case "Delivery Addresses":
			Webelement.getElementbyXpath("addressBtn").click();
			break;
		case "My Orders":
			Webelement.getElementbyXpath("odersBtn").click();
			break;
		case "Logout":
			Webelement.getElementbyXpath("logoutBtn").click();
			break;
		default: 
			throw new ElementNotVisibleException(item);
		}
	}
	
	public void enterPWD(String oldpwd, String newpwd, String confpwd) {
		Webelement.getElementbyXpath("curpwdTxt").sendKeys(oldpwd);
		Webelement.getElementbyXpath("newpwdTxt").sendKeys(newpwd);
		Webelement.getElementbyXpath("confpwdTxt").sendKeys(confpwd);
		Webelement.getElementbyXpath("changepwdBtn").click();
	}
	
	public void updateShippingInfo (String name, String phone, String address, String city) {
		Webelement.getElementbyXpath("editAddressBtn").click();
		Webelement.getElementbyXpath("fullnameAddressTxt").clear();
		Webelement.getElementbyXpath("fullnameAddressTxt").sendKeys(name);
		Webelement.getElementbyXpath("phonenumberAddressTxt").clear();
		Webelement.getElementbyXpath("phonenumberAddressTxt").sendKeys(phone);
		Webelement.getElementbyXpath("shipAddressTxt").clear();
		Webelement.getElementbyXpath("shipAddressTxt").sendKeys(address);
		Webelement.getElementbyXpath("shipCityTxt").clear();
		Webelement.getElementbyXpath("shipCityTxt").sendKeys(city);
	}
	
	public void selectAddressButton (String button) {
		switch (button) {
		case "Update":
			Webelement.getElementbyXpath("updateAddressBtn").click();
			break;
		case "Cancel":
			Webelement.getElementbyXpath("cancelAddressBtn").click();
			break;
		default: 
			throw new ElementNotVisibleException(button);
		}
	}
	
	public void assertUpdatingShipInfo (String name, String phone, String address, String city) {
		Webelement.getElementbyXpath("viewaddressFrm");
		assertEquals(name, Webelement.getElementbyXpath("fullnameAddressLbl").getText());
		assertEquals(phone, Webelement.getElementbyXpath("phonenumberAddressLbl").getText());
		assertEquals(address, Webelement.getElementbyXpath("shipAddressLbl").getText());
		assertEquals(city, Webelement.getElementbyXpath("shipCityLbl").getText());
	}
	
	public void assertFailShipInfo (String name, String phone, String address, String city) {
		Webelement.getElementbyXpath("viewaddressFrm");
		assertNotEquals(name, Webelement.getElementbyXpath("fullnameAddressLbl").getText());
		assertNotEquals(phone, Webelement.getElementbyXpath("phonenumberAddressLbl").getText());
		assertNotEquals(address, Webelement.getElementbyXpath("shipAddressLbl").getText());
		assertNotEquals(city, Webelement.getElementbyXpath("shipCityLbl").getText());
	}
	
	
	public void assertChangePWD () {
		Webelement.getElementbyXpath("successPage");
		Webelement.getElementbyXpath("successLink").click();
		enterPWD("C@123456", "chau123@","chau123@");
	}
	
	public void assertFailPWD () {
		Webelement.getElementbyXpath("errorchangepwdMsg");
	}
	
	public void assertLoginForm () {
		Webelement.getElementbyXpath("loginfrm");
	}
}
