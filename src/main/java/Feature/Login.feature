Feature: Login

Background:
Given Launching website "http://trucna.xyz" on the "Firefox" browser 

Scenario: Login with valid information
And Logging into the system with username "chaungo02" and password "chau123@"
Then Logging into the system is successful


Scenario: Login with invalid username
And Logging into the system with username "invalid" and password "chau123@"
Then Displaying error message


Scenario: Login with invalid password
And Logging into the system with username "chaungo02" and password "invalid"
Then Displaying error message


Scenario: Login with invalid username & password
And Logging into the system with username "invalid" and password "invalid"
Then Displaying error message


Scenario: Login with the empty username 
And Logging into the system with username " " and password "invalid"
Then Displaying required symbol at "Username"


Scenario: Login with the empty username 
And Logging into the system with username "invalid" and password ""
Then Displaying required symbol at "Password"


Scenario: Login with the empty username & password
And Logging into the system with username "" and password ""
Then Displaying required symbol at "Username"
Then Displaying required symbol at "Password"


