Feature: My Account

Background:
Given Launching website "http://trucna.xyz" on the "Firefox" browser 
And Logging into the system with username "chaungo06" and password "chau123@"
And User goes to My Account page


Scenario: Changing user password 
Given User selects "Personal" menu
And User changes the old password "chau123@" the new password "C@123456" and confirmation password "C@123456"
Then System navigates to the successful page


Scenario: Changing user password with invalid current password
Given User selects "Personal" menu
And User changes the old password "invalid123@" the new password "C@123456" and confirmation password "C@123456"
Then System displays error message


Scenario: Changing user password with the new one is incorrect format
Given User selects "Personal" menu
And User changes the old password "invalid123@" the new password "123456" and confirmation password "123456"
Then System displays error message


Scenario: Changing user password with the new password and the confirmation password are different
Given User selects "Personal" menu
And User changes the old password "chau123@" the new password "123456" and confirmation password "C@123456"
Then System displays error message


Scenario: Changing user password with empty information
Given User selects "Personal" menu
And User changes the old password "" the new password "" and confirmation password ""
Then System displays error message


Scenario: Changing shipping address successfully 
Given User selects "Delivery Addresses" menu
And User enters Fullname "Chau Ngo", Phone Number "0123456789", Shipping Address "18 ABC, 6" and Shipping city "HCMC"
And User selects "Update" button
Then System updates information successfully with new info Fullname "Chau Ngo", Phone Number "0123456789", Shipping Address "18 ABC, 6" and Shipping city "HCMC"


Scenario: Changing shipping address successfully 
Given User selects "Delivery Addresses" menu
And User enters Fullname "invalid", Phone Number "11111111", Shipping Address "invalid" and Shipping city "invalid"
And User selects "Cancel" button
Then System doesn't update information with Fullname "invalid", Phone Number "11111111", Shipping Address "invalid" and Shipping city "invalid"


Scenario: User log out the system
Given User selects "Logout" menu
Then System navigate to Login page