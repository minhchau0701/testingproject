@Test
Feature: Footer

Background:
Given Launching website "http://trucna.xyz" on the "Firefox" browser 


Scenario: Accessing to Footer Information - About Us
Given User selects "About Us" link 
And "About Us" page displays


Scenario: Accessing to Footer Information - Contact Us
Given User selects "Contact Us" link 
And "Contact Us" page displays

Scenario: Accessing to Footer Information - FQA's
Given User selects "FQA's" link 
And "FQA's" page displays

Scenario: Accessing to Footer Information - Special Products
Given User selects "Special Products" link 
And "Special Products" page displays