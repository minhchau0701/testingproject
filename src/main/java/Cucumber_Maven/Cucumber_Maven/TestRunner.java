package Cucumber_Maven.Cucumber_Maven;

import org.junit.runner.RunWith;

import Cucumber_Hooks.PropertiesFile;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions( 
		// -----------------------
		monochrome = true
		,features = "src/main/java/Feature"
		,glue={"Cucumber_Hooks","stepDefinition"}
		,tags= {"@Test"}
		//,dryRun = true
		)

public class TestRunner {
	

}
