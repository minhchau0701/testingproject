package handler;


import Cucumber_Hooks.*;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;



public class Login {


	
	public void loginSystem (String username, String password) {
		
		Webelement.getElementbyXpath("usernameTxt").sendKeys(username);
		Webelement.getElementbyXpath("passwordTxt").sendKeys(password);
		Webelement.getElementbyXpath("loginBtn").click();
		
	}
	
	public void assertRequiredField (String item) {
		WebElement element;
		switch (item) {
		case "Username":
			element = Webelement.getElementbyXpath("usernameRequired");
			break;
			
		case "Password":
			element = Webelement.getElementbyXpath("passwordRequired");
			break;
		
		default:
			throw new ElementNotVisibleException(item);
		}
		
	}
	
	public void assertLoginSuccess () {
		Webelement.getElementbyXpath("usernameHeader");
	}
	
	public void assertLoginFail () {
		Webelement.getElementbyXpath("loginfailMsg");
	}
	
	
	

}
