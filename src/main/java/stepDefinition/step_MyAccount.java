package stepDefinition;

import Cucumber_Maven.*;
import pages.*;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import handler.*;

public class step_MyAccount {
	
	
	@Given("^User goes to My Account page$")
	public void user_goes_to_My_Account_page()  {
		Myaccount page = new Myaccount();
		page.navigateAccountpage();
	}

	@Given("^User selects \"([^\"]*)\" menu$")
	public void user_selects_menu(String item)  {
		Myaccount page = new Myaccount();
		page.navigateSideMenu(item);
	}

	@Given("^User changes the old password \"([^\"]*)\" the new password \"([^\"]*)\" and confirmation password \"([^\"]*)\"$")
	public void user_changes_the_old_password_the_new_password_and_confirmation_password(String oldpwd, String newpwd, String confpwd)  {
		Myaccount page = new Myaccount();
		page.enterPWD(oldpwd, newpwd, confpwd);
	}

	
	@Given("^User enters Fullname \"([^\"]*)\", Phone Number \"([^\"]*)\", Shipping Address \"([^\"]*)\" and Shipping city \"([^\"]*)\"$")
	public void user_enters_Fullname_Phone_Number_Shipping_Address_and_Shipping_city(String name, String phone, String address, String city) {
		Myaccount page = new Myaccount();
		page.updateShippingInfo(name, phone, address, city);
	}
	
	@Given("^User selects \"([^\"]*)\" button$")
	public void user_selects_button(String button)  {
		Myaccount page = new Myaccount();
		page.selectAddressButton(button);
	}

	@Then("^System navigate to Login page$")
	public void system_navigate_to_Login_page()  {
		Myaccount page = new Myaccount();
		page.assertLoginForm();
	}

	@Then("^System updates information successfully with new info Fullname \"([^\"]*)\", Phone Number \"([^\"]*)\", Shipping Address \"([^\"]*)\" and Shipping city \"([^\"]*)\"$")
	public void system_updates_information_successfully_with_new_info_Fullname_Phone_Number_Shipping_Address_and_Shipping_city(String name, String phone, String address, String city)  {
		Myaccount page = new Myaccount();
		page.assertUpdatingShipInfo(name, phone, address, city);
	}

	@Then("^System doesn't update information with Fullname \"([^\"]*)\", Phone Number \"([^\"]*)\", Shipping Address \"([^\"]*)\" and Shipping city \"([^\"]*)\"$")
	public void system_doesn_t_update_information_with_Fullname_Phone_Number_Shipping_Address_and_Shipping_city(String name, String phone, String address, String city) {
		Myaccount page = new Myaccount();
		page.assertFailShipInfo(name, phone, address, city);
	}


	@Then("^System navigates to the successful page$")
	public void system_navigates_to_the_successful_page()  {
		Myaccount page = new Myaccount();
		page.assertChangePWD();
	}

	@Then("^System displays error message$")
	public void system_displays_error_message() {
		Myaccount page = new Myaccount();
		page.assertFailPWD();
	}
}
