package stepDefinition;

import Cucumber_Maven.*;
import pages.*;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import handler.*;

public class step_SeleniumTest {
	
	
	@Given("^Launching website \"([^\"]*)\" on the \"([^\"]*)\" browser$")
	public void launching_website_on_the_browser(String webURL, String browser) {
		Browser test = new Browser();
		test.launchBrowser(webURL, browser);
	}

	@Given("^Logging into the system with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void logging_into_the_system_with_username_and_password(String username, String password) {
		Homepage hp = new Homepage();
		hp.navigateLogin();
		Login test = new Login();
		test.loginSystem(username, password);
	}

	
	@Then("^Logging into the system is successful$")
	public void logging_into_the_system_is_successful() {
		Login test = new Login();
		test.assertLoginSuccess();
	}
	
	@Then("^Displaying error message$")
	public void displaying_error_message() {
		Login test = new Login();
		test.assertLoginFail();
	 
	}
	
	@Then("^Displaying required symbol at \"([^\"]*)\"$")
	public void displaying_tooltip_at(String item) {
		Login test = new Login();
		test.assertRequiredField(item);
	}


}
