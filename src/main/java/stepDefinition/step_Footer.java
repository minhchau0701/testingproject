package stepDefinition;

import Cucumber_Maven.*;
import pages.*;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import handler.*;

public class step_Footer {
	
	@Given("^User selects \"([^\"]*)\" link$")
	public void user_selects_link(String item) {
	    Homepage page = new Homepage();
	    page.navigateFooter(item);
	}

	@Given("^\"([^\"]*)\" page displays$")
	public void page_displays(String item)  {
		Homepage page = new Homepage();
	    page.displayPage(item);
	}
	
	
}
