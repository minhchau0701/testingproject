package Cucumber_Hooks;


import handler.Browser;
import Cucumber_Hooks.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Webelement {
	
	public static WebElement getElementbyXpath (String xPath) {
		PropertiesFile propertiesfile = new PropertiesFile();
		WebElement element = (new WebDriverWait(Browser.driver, 10))
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(propertiesfile.properties.getProperty(xPath))));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return element;
	}

}
